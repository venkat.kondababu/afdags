from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.models import Variable
from airflow.operators.python_operator import PythonOperator

from datetime import datetime, timezone, timedelta

default_args = {
    'start_date': datetime(2020, 8, 14),
    'owner': 'venkatk',
    'depends_on_past': False,
    'email': 'venkat.kondababu@intusurg.com',
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 0,
    'retry_delay': timedelta(minutes=60),
}

dag = DAG('test', default_args=default_args, schedule_interval = None, max_active_runs=1, catchup=False)

def print_timestamp():
    utc_dt = datetime.now(timezone.utc)
    print("Time display: 2:  ", utc_dt.astimezone().isoformat())

t1 = PythonOperator(task_id='test', python_callable=print_timestamp, dag=dag)

t1
